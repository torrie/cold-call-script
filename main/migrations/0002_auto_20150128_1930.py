# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='script',
            name='isis',
            field=models.BooleanField(default=False, verbose_name='Отображать скрипты категории?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='script',
            name='links_success',
            field=models.ManyToManyField(blank=True, verbose_name='Теги успеха', related_name='win', to='main.Script'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='script',
            name='links',
            field=models.ManyToManyField(blank=True, verbose_name='Теги для перехода', related_name='tags', to='main.Script'),
        ),
    ]
