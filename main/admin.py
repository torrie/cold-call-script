from django.contrib import admin
from main.models import Script, Category, Theme
# Register your models here.

class ScriptAdmin(admin.ModelAdmin):
    list_display = ('num', 'header', 'cat', 'tag_name', 'isis')

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('num', 'name', 'theme')

class ThemeAdmin(admin.ModelAdmin):
    list_display = ('num', 'name')

admin.site.register(Script, ScriptAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Theme, ThemeAdmin)