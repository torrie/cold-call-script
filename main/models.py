from django.db import models

# Create your models here.

class Theme(models.Model):
	num = models.IntegerField(verbose_name='Номер темы по порядку')
	name = models.CharField(max_length=255, verbose_name='Название темы')
	
	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Тема"
		verbose_name_plural = "Темы"


class Category(models.Model):
	num = models.IntegerField(verbose_name='Номер категории по порядку')
	name = models.CharField(max_length=255, verbose_name='Название')
	theme = models.ForeignKey(Theme, verbose_name='Тема', null=True)
	
	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Категория"
		verbose_name_plural = "Категории"


class Script(models.Model):
	num = models.IntegerField(verbose_name='Номер скрипта внутри категории')
	cat = models.ForeignKey(Category, verbose_name='Категория')
	
	header = models.CharField(max_length=255, verbose_name='Заголовок')
	text = models.TextField(verbose_name='Текст скрипта')
	
	links = models.ManyToManyField('self', symmetrical=False, blank=True, verbose_name='Теги для перехода', related_name="tags")
	tag_name = models.CharField(max_length=255, verbose_name='Как этот материал будет выглядить в теге')
	isis = models.BooleanField(verbose_name='Отображать скрипты категории?', default=False)

	links_success = models.ManyToManyField('self', symmetrical=False, blank=True, verbose_name='Теги успеха', related_name="win")

	def __str__(self):
		return "%s" % self.header

	class Meta:
		verbose_name = "Скрипт"
		verbose_name_plural = "Скрипты"
