from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sales.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	url(r'^$', 'main.views.root'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tinymce/', include('tinymce.urls')),
)
