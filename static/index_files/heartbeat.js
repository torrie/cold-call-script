//console.log('heartbeat');
jQuery(document).ready( function($) {
    //wp.heartbeat.interval( 'fast' );
    $(document).on( 'heartbeat-tick', function( event, data ) {
        //console.log(data);
        if ( !data.hasOwnProperty( 'wp-demo' ) || data['wp-demo'] === 0 || !data['wp-auth-check'] ) {
            authcheckL10n = null;
            window.location = wp_demo_expired_url;
        }
    });
});