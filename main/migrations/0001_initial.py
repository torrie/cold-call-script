# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('num', models.IntegerField(verbose_name='Номер категории по порядку')),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
            ],
            options={
                'verbose_name': 'Категория',
                'verbose_name_plural': 'Категории',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Script',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('num', models.IntegerField(verbose_name='Номер скрипта внутри категории')),
                ('header', models.CharField(max_length=255, verbose_name='Заголовок')),
                ('text', models.TextField(verbose_name='Текст скрипта')),
                ('tag_name', models.CharField(max_length=255, verbose_name='Как этот материал будет выглядить в теге')),
                ('cat', models.ForeignKey(to='main.Category', verbose_name='Категория')),
                ('links', models.ManyToManyField(to='main.Script', blank=True, verbose_name='Теги для перехода')),
            ],
            options={
                'verbose_name': 'Скрипт',
                'verbose_name_plural': 'Скрипты',
            },
            bases=(models.Model,),
        ),
    ]
