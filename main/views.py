from django.shortcuts import render
from main.models import Script, Category, Theme
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template.loader import render_to_string

# Create your views here.
def root(request):
	if 'id' in request.GET:
		idd = request.GET['id']
		theme = Theme.objects.get(id=idd)
		s = Script.objects.filter(cat__theme=theme).order_by('cat__num', 'num')
		c = Category.objects.filter(theme=theme)

		context = {
			'all': s,
			'cats': c,
			'theme': theme,

		}
		return HttpResponse(render_to_string('index.html', context))
	else:
		themes = Theme.objects.all()
		context = {
			'themes': themes,
		}
		return HttpResponse(render_to_string('all.html', context))