jQuery(document).ready(function($) {
    "use strict";

    $('body').on('click', '#wpdemo_delete_all_sandboxes', function() {
        $(this).attr('disabled', 'disabled')
        .html(ajax_object.wait_label);

        var data = {
    		action: 'wpdemo_delete_all_sandboxes',
            nonce: $(this).attr('data-nonce')
    	};

        var $this = $(this);
        jQuery.post(ajax_object.ajax_url, data, function(response) {
            response = parseInt(response);
            if ( !isNaN(response) ) {
                $this.removeAttr('disabled')
                .text(response + ' ' + ajax_object.done_label);

                $('#num_sandboxes').text(parseInt($('#num_sandboxes').text()) - response);
            } else {
                $this.removeAttr('disabled')
                .text(ajax_object.fail_label);
            }
    	});

        return false;
    });

    $('body').on('click', '#wpdemo_delete_child_sandboxes', function() {
        $(this).attr('disabled', 'disabled')
        .html(ajax_object.wait_label);

        var data = {
    		action: 'wpdemo_delete_child_sandboxes',
            nonce: $(this).attr('data-nonce')
    	};

        var $this = $(this);
        jQuery.post(ajax_object.ajax_url, data, function(response) {
            response = parseInt(response);
            if ( !isNaN(response) ) {
                $this.removeAttr('disabled')
                .text(response + ' ' + ajax_object.done_label);


                $('#num_sandboxes').text(parseInt($('#num_sandboxes').text()) - response);
            } else {
                $this.removeAttr('disabled')
                .text(ajax_object.fail_label);
            }
    	});

        return false;
    });


    $('body').on('click', '#wp-admin-bar-wpdemo_reset_sandbox_button a', function() {
        $(this).attr('disabled', 'disabled')

        var data = {
    		action: 'wpdemo_reset_sandbox',
    	};

        var $this = $(this);
        jQuery.post(ajax_object.ajax_url, data, function(response) {
            $this.removeAttr('disabled');
            window.location = ajax_object.admin_index;
    	});

        return false;
    });
});