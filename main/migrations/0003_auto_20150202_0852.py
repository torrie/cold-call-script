# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20150128_1930'),
    ]

    operations = [
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('num', models.IntegerField(verbose_name='Номер темы по порядку')),
                ('name', models.CharField(max_length=255, verbose_name='Название темы')),
            ],
            options={
                'verbose_name': 'Тема',
                'verbose_name_plural': 'Темы',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='category',
            name='theme',
            field=models.ForeignKey(null=True, to='main.Theme', verbose_name='Тема'),
            preserve_default=True,
        ),
    ]
